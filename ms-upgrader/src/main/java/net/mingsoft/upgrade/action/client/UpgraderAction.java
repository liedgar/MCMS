package net.mingsoft.upgrade.action.client;

import com.mingsoft.basic.action.*;
import org.springframework.stereotype.*;
import javax.servlet.http.*;
import com.mingsoft.util.*;
import com.mingsoft.base.constant.e.*;
import com.mingsoft.util.proxy.*;
import com.mingsoft.util.proxy.Header;

import org.springframework.web.bind.annotation.*;
import org.apache.http.message.*;
import org.apache.http.client.entity.*;
import org.apache.http.client.methods.*;
import java.util.zip.*;
import com.alibaba.druid.pool.*;
import org.apache.ibatis.jdbc.*;
import java.sql.*;
import org.apache.http.impl.client.*;
import org.apache.http.*;
import java.util.*;
import java.io.*;
import org.apache.http.client.*;

@Controller("upgrader")
@RequestMapping({ "/${managerPath}/upgrader" })
public class UpgraderAction extends BasicAction
{
    private Header header;
    private static final String MS_MSTORE_HOST = "http://ms.mingsoft.net/";
    private static final String UPDATE_PATH_TEM = "upgrader/";
    private String ck;
    private static final String UTF8 = "UTF-8";
    
    public UpgraderAction() {
        (this.header = new Header("mingsoft.net", "UTF-8")).setHeader("ms", "upgrader");
        this.header.setHeader("ver", "4.5.9");
    }
    
    @ResponseBody
    @RequestMapping(value = { "/sync" }, method = { RequestMethod.POST })
    public void sync(final HttpServletRequest request, final HttpServletResponse response) {
        this.header.setHeader("method", "sync");
        Map<String, Object> params = new HashMap<String, Object>();
        params = (Map<String, Object>)this.assemblyRequestMap(request);
        params.put("userHost", this.getUrl(request));
        final Result result = Proxy.post("http://ms.mingsoft.net//ms/sync.do", this.header, this.assemblyRequestMap(request), "UTF-8");
        final String content = result.getContent();
        if (!StringUtil.isBlank(content)) {
            this.outJson(response, (BaseEnum)null, true, content);
        }
        else {
            this.outJson(response, (BaseEnum)null, false);
        }
    }
    
    @ResponseBody
    @RequestMapping({ "/setup" })
    public void setup(final HttpServletRequest request) throws ClientProtocolException, IOException {
        this.header.setHeader("method", "setup");
        this.header.setCookie(request.getParameter("user"));
        Map<String, Object> params = new HashMap<String, Object>();
        params = (Map<String, Object>)this.assemblyRequestMap(request);
        params.put("userUrl", this.getUrl(request));
        final CloseableHttpClient httpclient = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://ms.mingsoft.net//people/upgrader/upgraderPeopleVersion/down.do");
        if (params != null) {
            final List list = new ArrayList();
            for (final String temp : params.keySet()) {
                list.add(new BasicNameValuePair(temp, (String)params.get(temp)));
            }
            try {
                httpPost.setEntity((HttpEntity)new UrlEncodedFormEntity(list, "UTF-8"));
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        if (this.header != null && this.header.getHeaders().size() > 0) {
            httpPost.setHeaders((org.apache.http.Header[])Proxy.assemblyHeader(this.header.getHeaders()));
        }
        final HttpResponse response = (HttpResponse)httpclient.execute((HttpUriRequest)httpPost);
        final HttpEntity entity = response.getEntity();
        final String destFileName = response.getHeaders("fileName")[0].getValue();
        final InputStream in = entity.getContent();
        final File file = new File(this.getRealPath(request, "upgrader//" + destFileName));
        try {
            final FileOutputStream fout = new FileOutputStream(file);
            int l = -1;
            final byte[] tmp = new byte[1024];
            while ((l = in.read(tmp)) != -1) {
                fout.write(tmp, 0, l);
            }
            fout.flush();
            fout.close();
        }
        finally {
            in.close();
        }
        in.close();
        httpclient.close();
        String sqlFile = "";
        String entryName = "";
        try {
            final ZipFile zipFile = new ZipFile(file);
            final File unzipFile = new File(this.getRealPath(request, "/"));
            final Enumeration<? extends ZipEntry> zipEnum = zipFile.entries();
            while (zipEnum.hasMoreElements()) {
                final ZipEntry entry = (ZipEntry)zipEnum.nextElement();
                try {
                    entryName = new String(entry.getName().getBytes("utf-8"));
                }
                catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
                if (entry.isDirectory()) {
                    new File(String.valueOf(unzipFile.getAbsolutePath()) + File.separator + entryName).mkdirs();
                }
                else {
                    try {
                        InputStream input = zipFile.getInputStream(entry);
                        OutputStream output = new FileOutputStream(new File(String.valueOf(unzipFile.getAbsolutePath()) + File.separator + entryName));
                        if (entryName.indexOf(".sql") > 0) {
                            sqlFile = String.valueOf(unzipFile.getAbsolutePath()) + File.separator + entryName;
                        }
                        final byte[] buffer = new byte[8192];
                        int readLen = 0;
                        while ((readLen = input.read(buffer, 0, 8192)) != -1) {
                            output.write(buffer, 0, readLen);
                        }
                        output.flush();
                        output.close();
                        input.close();
                        input = null;
                        output = null;
                    }
                    catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        catch (ZipException e4) {
            e4.printStackTrace();
        }
        catch (IOException e5) {
            e5.printStackTrace();
        }
        try {
            final DruidDataSource dds = (DruidDataSource)this.getBean(request.getServletContext(), "dataSource");
            final ScriptRunner runner = new ScriptRunner((Connection)dds.getConnection());
            runner.setErrorLogWriter((PrintWriter)null);
            runner.setLogWriter((PrintWriter)null);
            runner.runScript((Reader)new FileReader(new File(sqlFile)));
        }
        catch (FileNotFoundException e6) {
            e6.printStackTrace();
        }
        catch (SQLException e7) {
            e7.printStackTrace();
        }
    }
    
    @RequestMapping({ "/r" })
    public void r(final HttpServletRequest request, final HttpServletResponse response) {
        final String url = request.getParameter("url");
        this.ck = request.getParameter("ck");
        this.header.setCookie(this.ck);
        final Map<String, String> params = new HashMap<String, String>();
        final String userUrl = this.getUrl(request);
        params.put("userUrl", userUrl);
        final Result result = Proxy.post(url, this.header, (Map)params, "UTF-8");
        final String content = result.getContent();
        if (!StringUtil.isBlank(content)) {
            this.outString(response, (Object)content);
        }
    }
}
